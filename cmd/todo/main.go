package main

import (
	"fmt"
	"os"
	"todo/internal/model"
	"todo/internal/routes"
)

func main() {
	if os.Args[1] == "run" {
		fmt.Println("Starting web server.....")
		model.Setup()
		routes.SetupAndRun()
	} else {
		fmt.Println("This is a simple TODO web application, to run it type \"todo run\" ")

	}

}
