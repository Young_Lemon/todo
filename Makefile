BINARY_NAME=todo
DOCKER_TAG=todo
CONTAINER_NAME=${BINARY_NAME}
•DEFAULT_GOAL := run

build:
	go build -o ./bin/${BINARY_NAME} cmd/todo/main.go

run: build
	./bin/${BINARY_NAME}

clean: 
	go clean 
	rm -rf ./bin

docker-build:
	docker build . -t ${DOCKER_TAG}

docker-run: docker-build
	docker run -d --name ${CONTAINER_NAME}  -p 5001:5001 ${DOCKER_TAG}:latest

docker-clean:
	docker kill ${CONTAINER_NAME} || true
	docker rm ${CONTAINER_NAME} || true
	docker rmi ${DOCKER_TAG} || true