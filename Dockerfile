FROM golang:1.21.4-alpine as build
COPY . /src/
WORKDIR /src
RUN go build -o /src/todo cmd/todo/main.go

FROM alpine:3
WORKDIR /app
COPY --from=build /src/todo /app/
COPY --from=build /src/templates /app/templates
EXPOSE 5001
ENTRYPOINT ["/app/todo"]
CMD [ "run" ]